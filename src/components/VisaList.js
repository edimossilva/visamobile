import React from 'react';
import axios from 'axios';

import VisaShow from './VisaShow'
import {
  Text,
} from 'react-native';

export default class VisaList extends React.Component {
  state = {
    visas: [],
  }

  componentDidMount() {
    axios.get('http://192.168.1.73:8080/visas').then((response)=> {
      this.setState({ 
        visas: response.data
      });
    }).catch((error) => {
      console.log(error.response)
    });
  }
  
  render() {
    return (
      <>
        <Text>VisaList</Text>
        { this.state.visas.map(v => <VisaShow visa={v} key={ v.id }></VisaShow>) }
      </>
    );
  }
}