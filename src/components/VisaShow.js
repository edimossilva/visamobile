import React from 'react';
import {
  Text,
  TouchableOpacity
} from 'react-native';

export default class Visa extends React.Component {
  
  constructor(props) {
    super(props)
    name = props.visa.name;
  }

  showVisa() {
    console.log(name);
  }

  render() {
    return (
      <TouchableOpacity onPress={ this.showVisa }> 
        <Text> { name } </Text>
      </TouchableOpacity>
    )
  }
}