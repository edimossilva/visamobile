/**
 * @format
 */

import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import axios from 'axios';
import VisaList from '../../src/components/VisaList';

jest.mock('axios');

describe('General behavior', () => {
  let wrapper;

  const response = {
    data: [
      { id:1, name:"Work Visa" },
      { id:2, name:"Tourist Visa" }   
    ]
  }
  
  beforeEach(async () => {
    axios.get.mockImplementation(() => Promise.resolve(response));
    wrapper = await shallow(<VisaList />);
  });

  it('match tree snapshoot ', async() => {
    await expect(wrapper).toMatchSnapshot();
  });
  
  it('match state snapshoot ', async() => {
    await expect(wrapper.state()).toMatchSnapshot();
  });
});
